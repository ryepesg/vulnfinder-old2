# Vulnfinder
Welcome to Vulnfinder, please following the links to see the available documentation:
 * [How to install](https://gitlab.com/ryepesg/vulnfinder/wikis/how-to-install)
 * [How to use](https://gitlab.com/ryepesg/vulnfinder/wikis/beginners-guide)